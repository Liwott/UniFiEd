\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{UniFiEd_TdG}[22/09/2020 Universe, Fields and Education : Theorie des Groupes]

\RequirePackage{etoolbox}
\ifdefvoid{\UniFiEd}{
\def\UniFiEd{UniFiEd}
}{}

\PassOptionsToClass{french}{\UniFiEd}

\newtoggle{UFEexam}
\togglefalse{UFEexam}
\DeclareOption{exam}{%
\toggletrue{UFEexam}
\PassOptionsToClass{\UniFiEd}{remarks}
}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{\UniFiEd}}

\ProcessOptions\relax
\LoadClass{\UniFiEd}

\def\UFEnumber{}
\iftoggle{UFEexam}{
\title{Examen de Th\'eorie des Groupes -- S-PHYS-201}
\def\UFEremarkI{\item indiquez votre nom sur chaque feuille}
\def\UFEremarkII{\item justifiez les r\'eponses}
\def\UFEremarkIII{\item les r\'eponses attendues ne doivent pas \^etre scind\'ees et doivent tenir sur les deux doubles feuilles distribu\'ees}
\def\UFEremarkIIII{\item examen \`a livre ferm\'e}
\def\UFEremarks{\UFEremarkI\UFEremarkII\UFEremarkIII\UFEremarkIIII}
}{
\title{Th\'eorie des Groupes -- S-PHYS-201 : S\'eance \UFEnumber.}
}
