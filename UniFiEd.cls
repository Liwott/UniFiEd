\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{UniFiEd}[22/09/2020 Universe, Fields and Education]

\LoadClass{article}

\RequirePackage{etoolbox}
\RequirePackage{fp}

%correction mode
\newtoggle{UFEcorrection}
\togglefalse{UFEcorrection}
\DeclareOption{correction}{\toggletrue{UFEcorrection}}

%mode allowing for preliminary remarks in box after title
\newtoggle{UFEremarks}
\togglefalse{UFEremarks}
\DeclareOption{remarks}{\toggletrue{UFEremarks}}

%mode allowing to display marks
%fp package is used to allow for decimal marks
\newtoggle{UFEmarks}
\togglefalse{UFEmarks}
\def\UFEmark#1{}
\FPset\UFEglobalmark{0}
\FPset\UFEquestionmark{0}
\DeclareOption{marks}{\toggletrue{UFEmarks}
\def\UFEmark#1{\textbf{[#1\ \UFEWpoints]}
\FPeval\UFEglobalmark{clip(UFEglobalmark+#1)}
\xdef\UFEglobalmark{\UFEglobalmark}
\FPeval\UFEquestionmark{clip(UFEquestionmark+#1)}
}
\AtEndDocument{\\\textbf{\UFEWtotal : \UFEglobalmark\ \UFEpoints}}
}

%mode enabling comments for staff only
\newtoggle{UFEstaff}
\togglefalse{UFEstaff}
\DeclareOption{staff}{\toggletrue{UFEstaff}}

%options and packages
\DeclareOption{french}{
\PassOptionsToPackage{french}{babel}
\def\UFEWremarks{Remarques : }
\def\UFEWhint{Indice : }
\def\UFEWstaffcomment{Commentaire interne : }
\def\UFEWpoints{point(s)}
\def\UFEWquestion{Question}
\def\UFEWtotal{Total}
}
\DeclareOption{english}{
\PassOptionsToPackage{english}{babel}
\def\UFEWremarks{Remarks : }
\def\UFEWhint{Hint : }
\def\UFEWstaffcomment{Staff comment : }
\def\UFEWpoints{point(s)}
\def\UFEWquestion{Question}
\def\UFEWtotal{Total}
}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

\ProcessOptions\relax

%\RequirePackage{babel}
\RequirePackage[utf8x]{inputenc}
\RequirePackage[T1]{fontenc}

%packages required for sectioning as it is now
%for environ, cf https://tex.stackexchange.com/questions/14616/environments-with-conditionals-in-why-doesnt-this-work
\RequirePackage[inline]{enumitem}
\RequirePackage{environ}

%packages required for layout
\RequirePackage[top=3cm,bottom=2cm,left=2.5cm,right=2cm]{geometry}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}

%
\RequirePackage{hyperref}

%layout
\fancyhf{}
\pagestyle{fancy}
\cfoot{\thepage / \pageref*{LastPage}}
\renewcommand{\footrulewidth}{.4pt}
\renewcommand{\baselinestretch}{1.4}
\setlength{\parindent}{0cm}

%remarks, typically rules for exam
\def\UFEremarks{}
\def\UFEaddremark#1{\appto\UFEremarks{\item #1}}

%entete
\makeatletter
\AtBeginDocument{%
\begin{flushleft}
{\Large{\textbf{\@title}}}
\end{flushleft}
\begin{flushright}
\@date%
\end{flushright}
\iftoggle{UFEremarks}{
\iftoggle{UFEcorrection}{}{%
\begin{center}
============================================================
\textbf{\UFEWremarks}
\begin{enumerate*}[label=(\alph*)]
  \UFEremarks
\end{enumerate*}
============================================================
\end{center}
}}{}
}

%question counter
\newcounter{UFEquestion}
\setcounter{UFEquestion}{0}
\newcounter{UFEsubquestion}

%sectioning
\iftoggle{UFEcorrection}{
%% correction mode
\newenvironment{UFEquestion}{%
\ifnumcomp{\theUFEquestion}{=}{0}{}{\newpage}
\addtocounter{UFEquestion}{1}
\setcounter{UFEsubquestion}{0}
\section*{\UFEWquestion \theUFEquestion.}
}{%
\iftoggle{UFEmarks}{\,\\\textbf{\UFEWtotal \UFEWquestion \theUFEquestion : \UFEquestionmark\ \UFEWpoints}}{}
}
%
\NewEnviron{UFEproblem}{\textit{\BODY}}
%
\NewEnviron{UFEsubquestion}{%
\addtocounter{UFEsubquestion}{1}
\subsection*{(\alph{UFEsubquestion}) \BODY}}
%
\NewEnviron{UFEanswer}{\BODY}
%
\NewEnviron{UFEhint}{\iffalse\expandafter\BODY\fi}
%
}{
%% uncorrected mode
\newenvironment{UFEquestion}{%
\addtocounter{UFEquestion}{1}
\setcounter{UFEsubquestion}{0}
\subsubsection*{\UFEWquestion \theUFEquestion.}
}{%\
\iftoggle{UFEmarks}{\textbf{\UFEWtotal \UFEWquestion \theUFEquestion : \UFEquestionmark\ \UFEWpoints}
}{
}}
%
\NewEnviron{UFEproblem}{\nopagebreak\textit\BODY}
%
\NewEnviron{UFEsubquestion}{%
\nopagebreak
\addtocounter{UFEsubquestion}{1}
\begin{itemize}[topsep=-0.2ex]
  \item[(\alph{UFEsubquestion})]\BODY
\end{itemize}}
%
\NewEnviron{UFEanswer}{\nopagebreak\iffalse\expandafter\BODY\fi}
%
\NewEnviron{UFEhint}{\nopagebreak
\begin{itemize}[topsep=-1ex]
  \item[]\it\textbf{\UFEWhint}\BODY
\end{itemize}}
%
}

%comments that only appear in staff mode
\iftoggle{UFEstaff}{
\NewEnviron{UFEstaffcomment}{\nopagebreak
\begin{itemize}[topsep=-1ex]
  \item[]\it\textbf{\UFEWstaffcomment}\BODY
\end{itemize}}
}{
\NewEnviron{UFEstaffcomment}{\iffalse\expandafter\BODY\fi}
}
