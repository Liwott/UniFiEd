\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{UniFiEd_MA}[22/09/2020 Universe, Fields and Education : Mecanique Analytique]

\RequirePackage{etoolbox}
\ifdefvoid{\UniFiEd}{
\def\UniFiEd{UniFiEd}
}{}

\PassOptionsToClass{french}{\UniFiEd}

\newtoggle{UFEexam}
\togglefalse{UFEexam}
\DeclareOption{exam}{%
\toggletrue{UFEexam}
\PassOptionsToClass{\UniFiEd}{remarks}
}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{\UniFiEd}}

\ProcessOptions\relax
\LoadClass{\UniFiEd}

\def\UFEnumber{}
\iftoggle{UFEexam}{
\title{Examen de M\'ecanique Analytique -- S-PHYS-017}
\def\UFEremarkI{\item indiquez votre nom sur chaque feuille}
\def\UFEremarkII{\item justifiez les r\'eponses}
\def\UFEremarkIII{\item les r\'eponses attendues ne doivent pas \^etre scind\'ees et doivent tenir sur les deux doubles feuilles distribu\'ees}
\def\UFEremarkIIII{\item examen \`a livre ferm\'e}
\def\UFEremarks{\UFEremarkI\UFEremarkII\UFEremarkIII\UFEremarkIIII}
}{
\title{M\'ecanique Analytique -- S-PHYS-017 : S\'eance \UFEnumber.}
}
