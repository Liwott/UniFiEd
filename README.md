# UniFiEd

**UniFiEd** is a LaTeX class designed by David De Filippi to create exercise sheets (for tutoring sessions or exams) and their solutions from a single source file.
The name of the class stands for two things :
- **Universe, Fields and Education**, a fictitious "educational section" of the existing unit "Physics of the Universe, Fields and Gravitation" of UMONS where research in high energy theoretical physics is carried on. This is in particular the unit in charge (as of 2020) of the courses of analytical mechanics and group theory, for which this class was designed.
- *Unifying* the source file of a given exercise sheet and its correction. *Unifying* the templates for tutoring sessions and exams. Generalising to other courses the class `GroupTheoryUMONS` , thereby *unifying* the templates for different lectures.

Aside from its own options, all the objects (macros, environments, counters, toggles) defined in the class are prefixed with `UFE` (the 3 capitals in the name) to make it as compatible as possible with other packages.

#### Document Structure

A document written with this class is organised as a collection of environments and subenvironments:
- **UFEquestion** is the coarser division of a document. All the following environments should be included in a `UFEquestion`;
- **UFEproblem** typically comes at the beginning of a `UFEquestion`, to expose the setting of the problem. Can come again between two `UFEsubquestion` to broaden the general setting of the `UFEquestion`;
- **UFEsubquestion** is specific step of a `UFEquestion`;
- **UFEanswer** *follows* a given `UFEsubsection`, and contains its resolution. It is only displayed in the dedicated *correction* mode;
- **UFEhint** typically come to bring a tip to solve a given `UFEsubquestion`. It is not displayed in *correction* mode.

A title needs to be provided for the document via the standard `\title`.

#### Class Options

There is a variety of options activated different (all compatible) modes:
- **correction** allows to see the content of the UFEanswer's;
- **remarks** allows to add, via `\UFEaddremark`, remarks in the stylised preamble of the sheet, typically used for enunciating the rules for an exam;
- **marks** allow to attribute marks (via `\UFEmark`) to subquestions, or anywhere in the text. Totals are calculated after each question and at the very end of the document.
- **staff** allows to display comments (added via `\UFEstaffcomment`) for the eyes of the staff only.

Each of these options has an associated `toggle` (defined in the [etoolbox](https://www.ctan.org/pkg/etoolbox) package) that allows customized editing, for example a line break specific to correction mode. For example, for an option called `example`, one can write
```
\iftoggle{UFEexample}{code executed with "example" option}{code executed without "example" option}
```

Aside from these options, there is an extra option (`english` or `french`) for language, which defines words through macros prefixed with `UFEW`. All the other options are passed down to `article`, of which it is a subclass.

#### Subclasses

There are so far 2 course-specific subclasses:
- `UniFiEd_MA` for the course **S-PHYS-017 Mécanique Analytique**
- `UniFiEd_TdG` for the course **S-PHYS-201 Théorie des Groupes**

If `UniFiEd` is not in the default path, its path can be provided before loading the subclass by defining the command `\UniFiEd`. 

Both set the language to `french`, and have an extra option
- **exam** includes `remarks` and redefines the title

A default title is also provided without this option, assumed that the tutoring sessions are numbered and that the user provides the number in question via `\UFEnumber`.

They pass all other options down to `UniFiEd`.

#### Dependencies

The following packages are used in the code:
- `etoolbox` for toggles and hooks
- `fp` for non-integer marks
- `inputenc` and `fontenc`
- `enumitem` and `environ` for sectioning
- `geometry`, `fancyhdr` and `lastpage` for the layout
- `hyperref`

#### Acknowledgements

I wrote the package while a F.R.S-FNRS Research Fellow.
I would like to thank University of Mons and in particular Professors Nicolas Boulanger and Yves Brihaye for the teaching opportunity that pushed me to write the package.